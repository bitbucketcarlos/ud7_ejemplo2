package com.example.ud7_ejemplo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ud7_ejemplo2.databinding.ActivityMainBinding
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root

        setContentView(view)

        // Buscamos el ViewPager y el TabLayout
        val viewPager = binding.viewpager
        val tabLayout = binding.tabs

        // Creamos y asignamos el adaptador de Fragments
        val adapter = AdapterFragments(this)

        viewPager.adapter = adapter

        // Conectamos el ViewPager con el TabLayout mediante la clase TabLayoutMediator
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            when (position) {
                0 -> tab.text = getString(R.string.fragment1)
                1 -> tab.text = getString(R.string.fragment2)
                2 -> tab.text = getString(R.string.fragment3)
            }
        }.attach()
    }
}